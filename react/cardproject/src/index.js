import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Site from './main/Site';
import * as serviceWorker from './serviceWorker';



ReactDOM.render(<Site/>, document.getElementById('content'));



serviceWorker.unregister();
