import React, { Component } from 'react'

import './Site.css'

import CardList from '../components/Misc/CardList'


  
export default class Site extends Component {
    constructor(props){
        super(props);
        this.state = {
            id:0,
            cards:[{id:0, title:"", text:"", deleted: 0}],
            sortorder:"oldest"
        }
    }
    addCard(){
        this.state.cards.push({id:this.state.id+1,title:"", text:"", deleted: 0});
        this.setState({cards:this.state.cards, id:this.state.id+1});
    }
    handleChange(event, id){
        if(event.target.id==="orderby"){
            this.setState({sortorder: event.target.value});
        }else if(event.target.id==="titulo"){
            let cardsClone = this.state.cards;
            let idx = cardsClone.findIndex(card => card.id === id);
            cardsClone[idx].title = event.target.value;
            this.setState({cards: cardsClone});
        }else if(event.target.id==="texto"){
            let cardsClone = this.state.cards;
            let idx = cardsClone.findIndex(card => card.id === id);
            cardsClone[idx].text = event.target.value;
            this.setState({cards: cardsClone});
        }else if(event.target.id==="delete"){
            //let cardsClone = this.state.cards;
            let n=0;
            while(n<this.state.cards.length && this.state.cards[n].id!=id){
                n++;
            }
            //for(let idx=n;idx<this.state.cards.length-1;idx++){
            //    //cardsClone[idx].id = cardsClone[idx+1].id;
            //    cardsClone[idx].title = cardsClone[idx+1].title;
            //    cardsClone[idx].text = cardsClone[idx+1].text;
            //}
            //cardsClone.pop();
            //this.state.id--;
            //this.setState({cards: cardsClone});
            this.state.cards[n].deleted = 1;
            this.setState({cards: this.state.cards});

        }
    }
    render(){
        return(
            <div>
                <div id="midbar" className="mid">
                    <div id="buttonNewIdea" className="menuoption">
                        <button onClick={this.addCard.bind(this)} className="button">New Idea</button>
                    </div>
                    <div>
                        <div className="menuoption">
                            <span className="combobox">Sort ideas by: </span>
                            <select id="orderby" className="combobox" onChange={this.handleChange.bind(this)}>
                                <option value="oldest">Oldest</option>
                                <option value="newest">Newest</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <CardList cardlist={this.state.cards} sortedby={this.state.sortorder} changeHandler={this.handleChange.bind(this)}/>
                </div>
            </div>
        )
    }
}