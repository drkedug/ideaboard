import React, { Component } from 'react'
import './Card.css'


export default class Card extends Component {
    constructor(props){
        super(props);
        this.state = {
            id:this.props.id,
            title:this.props.titulo,
            text:this.props.textarea,
            deleted: 0
        }
        this.handleChange = this.handleChange.bind(this);
        this.setState({update: this.update})
    }
    handleChange(event){
        if(event.target.id==="delete"){
            this.setState({deleted: 1});
        }else if(event.target.id==="titulo"){
            this.setState({title: event.target.value});
        }else if(event.target.id==="texto"){
            this.setState({text: event.target.value});
        }
        
        this.props.changeHandler(event, this.state.id);
    }
    render(){
        return(
        <div className="card">
            <div className="cardtitle">
                <input id="titulo" type="text" value={this.state.title || ''} onChange={this.handleChange} onUpdate={this.update}/>
            </div>
            <div>
                <button id="delete" className="deletebutton" onClick={this.handleChange}>X</button>
            </div>
            <div className="cardtext">
                <textarea id="texto" type="text" value={this.state.text || ''} onChange={this.handleChange}/>
            </div>
        </div>
        );
    }
}