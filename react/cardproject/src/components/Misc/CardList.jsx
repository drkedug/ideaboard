import React, { Component } from 'react'
import './CardList.css'
import Card from '../Misc/Card'

export default class CardList extends Component{ 
    render(){   
        const items = [];
         if(this.props.sortedby==="oldest"){
            for(let i=0;i<this.props.cardlist.length;i++){
                if(this.props.cardlist[i].deleted==0){
                    items.push(<Card key={this.props.cardlist[i].id} id={this.props.cardlist[i].id} titulo={this.props.cardlist[i].title} textarea={this.props.cardlist[i].text} changeHandler={this.props.changeHandler}/>)
                }
            }
         }else if(this.props.sortedby==="newest"){
            for(let i=this.props.cardlist.length-1;i>=0;i--){
                if(this.props.cardlist[i].deleted==0){
                    items.push(<Card key={this.props.cardlist[i].id} id={this.props.cardlist[i].id} titulo={this.props.cardlist[i].title} textarea={this.props.cardlist[i].text} changeHandler={this.props.changeHandler}/>)
                }
            }
         }
    return (
        <div className ="cardpool" id = "cardpool">
            {items}
        </div>
      )
    }
}