import React from 'react'
import './NewIdeaButton.css'

export default props => {
     
    
    return(
    <button className="button" 
    onClick={e=> props.click && props.click(e.target.innerHTML)}>
 
      {props.label}
    </button>
    
    );
    
    

}
